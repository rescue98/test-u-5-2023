# Estructura Interna

Este proyecto corresponde a una red neuronal basada en la siguiente estructura
![Diagrama de Clases](spec/clases.jpg)

# Red Neuronal
La red neuronal fue entrenada con el set de datos [wine.Wine](http://archive.ics.uci.edu/ml/datasets/Wine) que puede encontrar en la carpeta "resources/data"con los siguientes atributos:
1) Alcohol 
2) Ácido málico 
3) Ceniza 
4) Alcalinidad de la ceniza 
5) Magnesio 
6) Fenoles totales 
7) Flavonoides 
8) Fenoles no flavonoides 
9) Proantocianinas 
10) Intensidad de color 
11) Tono 
12) DO280 / DO315 diluidos
13) Prolina
