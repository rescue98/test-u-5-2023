package app;

import model.Network;
import wine.Wine;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    private static final Logger LOGGER = Logger.getLogger("App.App");
    public static void main(String[] args) {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        if (LOGGER.isLoggable(Level.ALL)) {
            LOGGER.log(Level.ALL, String.format("%d correct of %d = %.2f%%",
                    results.correct, results.trials, results.percentage * 100));
        }
    }
}
