package model;

import util.Util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;

public class Layer {
	static Optional<Layer> previousLayer;
	private static final List<Neuron> neurons = new ArrayList<>();
	private static double[] outputCache;

	public Layer(Optional<Layer> previousLayer, int numNeurons, double learningRate,
				 DoubleUnaryOperator activationFunction, DoubleUnaryOperator derivativeActivationFunction) {
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < numNeurons; i++) {
			double[] randomWeights = null;
			if (previousLayer.isPresent()) {
				randomWeights = random.doubles(neurons.size()).toArray();
			}
			Neuron neuron = new Neuron(randomWeights, learningRate, activationFunction, derivativeActivationFunction);
			neurons.add(neuron);
		}
	}

	public static double[] outputs(double[] inputs) {
		if (previousLayer.isPresent()) {
			outputCache = neurons.stream().mapToDouble(n -> Neuron.output(inputs)).toArray();
		} else {
			outputCache = inputs;
		}
		return outputCache;
	}

	public static void calculateDeltasForOutputLayer(double[] expected) {
		for (int n = 0; n < neurons.size(); n++) {
			Neuron.setDelta(neurons.get(n).derivativeActivationFunction.applyAsDouble(Neuron.getOutputCache())
					* (expected[n] - outputCache[n]));
		}
	}

	public void calculateDeltasForHiddenLayer() {
		for (int i = 0; i < neurons.size(); i++) {
			int index = i;
			double[] nextWeights = neurons.stream().mapToDouble(n -> Neuron.getWeights()[index]).toArray();
			double[] nextDeltas = neurons.stream().mapToDouble(n -> Neuron.getDelta()).toArray();
			double sumWeightsAndDeltas = Util.dotProduct(nextWeights, nextDeltas);
			Neuron.setDelta(neurons.get(i).derivativeActivationFunction
					.applyAsDouble(Neuron.getOutputCache()) * sumWeightsAndDeltas);
		}
	}
	public static List<Neuron> getNeurons() {
		return neurons;
	}
	public static double[] getOutputCache() {
		return outputCache;
	}
}
