package model;

import util.Util;

import java.util.function.DoubleUnaryOperator;

public class Neuron {
	private static double[] weights;
	public final double learningRate;
	private static double outputCache;
	private static double delta;
	private static DoubleUnaryOperator activationFunction;
	public final DoubleUnaryOperator derivativeActivationFunction;

	public Neuron(double[] weights, double learningRate, DoubleUnaryOperator activationFunction,
			DoubleUnaryOperator derivativeActivationFunction) {
		Neuron.weights = weights;
		this.learningRate = learningRate;
		Neuron.activationFunction = activationFunction;
		this.derivativeActivationFunction = derivativeActivationFunction;
	}

	public static double output(double[] inputs) {
		outputCache = Util.dotProduct(inputs, weights);
		return activationFunction.applyAsDouble(outputCache);
	}

	public static double[] getWeights() {
		return weights;
	}

	public static double getOutputCache() {
		return outputCache;
	}

	public static double getDelta() {
		return delta;
	}

	public static void setDelta(double delta) {
		Neuron.delta = delta;
	}

}
