package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

public class Network<T> {
	private final List<Layer> layers = new ArrayList<>();

	public Network(int[] layerStructure, double learningRate,
			DoubleUnaryOperator activationFunction, DoubleUnaryOperator derivativeActivationFunction) {
		if (layerStructure.length < 3) {
			throw new IllegalArgumentException("Error: Should be at least 3 layers (1 input, 1 hidden, 1 output).");
		}
		// input layer
		Layer inputLayer = new Layer(Optional.empty(), layerStructure[0], learningRate, activationFunction,
				derivativeActivationFunction);
		layers.add(inputLayer);
		// hidden layers and output layer
		for (int i = 1; i < layerStructure.length; i++) {
			Layer nextLayer = new Layer(Optional.of(layers.get(i - 1)), layerStructure[i], learningRate,
					activationFunction,
					derivativeActivationFunction);
			layers.add(nextLayer);
		}
	}

	private double[] outputs(double[] input) {
		double[] result = input;
		for (Layer ignored : layers) {
			result = Layer.outputs(result);
		}
		return result;

	}

	private void backpropagate(double[] expected) {
		// calculate delta for output layer neurons
		int lastLayer = layers.size() - 1;
		Layer.calculateDeltasForOutputLayer(expected);
		// calculate delta for hidden layers in reverse order
		for (int i = lastLayer - 1; i >= 0; i--) {
			layers.get(i).calculateDeltasForHiddenLayer();
		}
	}

	/**
	 * backpropagate() doesn't actually change any weights
	 * 	 this function uses the deltas calculated in backpropagate() to
	 * 	 actually make changes to the weights
	 */
	private void updateWeights() {
		for (Layer layer : layers.subList(1, layers.size())) {
			for (Neuron neuron : Layer.getNeurons()) {
				for (int w = 0; w < Neuron.getWeights().length; w++) {
					Neuron.getWeights()[w] = Neuron.getWeights()[w] + (neuron.learningRate *
							Layer.getOutputCache()[w] * Neuron.getDelta());
				}
			}
		}
	}

	public void train(List<double[]> inputs, List<double[]> expecteds) {
		for (int i = 0; i < inputs.size(); i++) {
			double[] xs = inputs.get(i);
			double[] ys = expecteds.get(i);
			outputs(xs);
			backpropagate(ys);
			updateWeights();
		}
	}

	public class Results {
		public final int correct;
		public final int trials;
		public final double percentage;

		public Results(int correct, int trials, double percentage) {
			this.correct = correct;
			this.trials = trials;
			this.percentage = percentage;
		}
	}


	public Results validate(List<double[]> inputs, List<T> expecteds, Function<double[], T> interpret) {
		int correct = 0;
		for (int i = 0; i < inputs.size(); i++) {
			double[] input = inputs.get(i);
			T expected = expecteds.get(i);
			T result = interpret.apply(outputs(input));
			if (result.equals(expected)) {
				correct++;
			}
		}
		double percentage = (double) correct / (double) inputs.size();
		return new Results(correct, inputs.size(), percentage);
	}
}
